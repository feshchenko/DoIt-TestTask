//
//  DTPicture.swift
//  DoIt-TestTask
//
//  Created by Arthur Feshchenko on 10/1/17.
//  Copyright © 2017 Arthur Feshchenko. All rights reserved.
//

import UIKit

struct DTPicture {
  let id: UInt64
  let description: String
  let hashtag: String
  let weather: String
  let longitude: Double
  let latitude: Double
  let smallImagePath: String
  let bigImagePath: String
  var image: UIImage!
}
