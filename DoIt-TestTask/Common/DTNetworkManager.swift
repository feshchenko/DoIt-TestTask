//
//  DTNetworkManager.swift
//  DoIt-TestTask
//
//  Created by Arthur Feshchenko on 9/29/17.
//  Copyright © 2017 Arthur Feshchenko. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class DTNetworkManager: NSObject {
  
  static let instance = DTNetworkManager()
  static let apiAddress = "http://api.doitserver.in.ua"
  
  var sessionManager: SessionManager!
  
  private override init() {
    sessionManager = nil
  }
  
  private func createSessionManager(withToken tokenString: String) {
    let config = URLSessionConfiguration.default
    var defaultHeaders = SessionManager.defaultHTTPHeaders
    defaultHeaders.updateValue(tokenString, forKey: "token")
    config.httpAdditionalHeaders = defaultHeaders
    
    self.sessionManager = SessionManager(configuration: config)
  }
  
  func registerWith(email: String, username: String? = nil, password: String, avatar: URL, completionHandler completion: @escaping (Bool, String?, String?) -> Void) {
    var params: [String: String] = [:]
    params["email"] = email
    params["password"] = password
    if let username = username {
      params["username"] = username
    }
    
    let headers = ["Content-type": "multipart/form-data"]
    
    SessionManager.default.upload(multipartFormData: { (multipartData) in
      for (key, value) in params {
        multipartData.append(value.data(using: String.Encoding.utf8)!, withName: key)
      }
      multipartData.append(avatar, withName: "avatar")
    }, to: "\(DTNetworkManager.apiAddress)/create", method: .post, headers: headers) { (result) in
      switch result {
      case .success(let request, _, _):
        request.validate(statusCode: [201]).responseJSON() { response in
          switch (response.result) {
          case .success(let value):
            let responseData = JSON(value)
            let tokenString = responseData["token"].stringValue
            let avatarURL = responseData["avatar"].stringValue
            self.createSessionManager(withToken: tokenString)
            completion(true, tokenString, avatarURL)
            break
          case .failure(let error):
            if ((response.response?.statusCode)! == 400) {
              let errorData = JSON(response.data!)
              if let emailErrors = errorData["children"]["email"]["errors"].array {
                for errorString in emailErrors {
                  print(errorString.stringValue)
                }
              }
              if let passwordErrors = errorData["children"]["password"]["errors"].array {
                for errorString in passwordErrors {
                  print(errorString.stringValue)
                }
              }
              if let avatarErrors = errorData["children"]["avatar"]["errors"].array {
                for errorString in avatarErrors {
                  print(errorString.stringValue)
                }
              }
            } else {
              print("Response status: \((response.response?.statusCode)!), \(error.localizedDescription)")
            }
            completion(false, nil, nil)
            break
          }
        }
        break
      case .failure(let error):
        print("Failed encoding data for registration: \(error.localizedDescription)")
        completion(false, nil, nil)
        break
      }
    }
  }
  
  func loginWith(email: String, password: String, completionHandler completion: @escaping (Bool, String?, String?) -> Void) {
    let params = ["email": email, "password": password]
    
    SessionManager.default.request("\(DTNetworkManager.apiAddress)/login", method: .post, parameters: params, encoding: URLEncoding.httpBody).validate(statusCode: [200]).responseJSON() { response in
      switch response.result {
      case .success(let value):
        let responseData = JSON(value)
        let tokenString = responseData["token"].stringValue
        let avatarURL = responseData["avatar"].stringValue
        self.createSessionManager(withToken: tokenString)
        completion(true, tokenString, avatarURL)
        break
      case .failure(let error):
        print("Error on login: \(error.localizedDescription)")
        let responseData = JSON(response.data!)
        print(responseData["error"].string ?? "")
        break
      }
    }
  }
  
  func getImagesWith(comletionHandler completion: @escaping (Bool, [DTPicture]?) -> Void) {
    self.sessionManager.request("\(DTNetworkManager.apiAddress)/all", method: .get).validate(statusCode: [200]).responseJSON(queue: DispatchQueue.global(qos: .background)) { response in
      switch response.result {
      case .success(let value):
        let picturesData = JSON(value)
        var pictures: [DTPicture] = []
        for pictureData in picturesData["images"].arrayValue {
          var picture = DTPicture(id: pictureData["id"].uInt64Value,
                                  description: pictureData["description"].stringValue,
                                  hashtag: pictureData["hashtag"].stringValue,
                                  weather: pictureData["parameters"]["weather"].stringValue,
                                  longitude: pictureData["parameters"]["longitude"].doubleValue,
                                  latitude: pictureData["parameters"]["latitude"].doubleValue,
                                  smallImagePath: pictureData["smallImagePath"].stringValue,
                                  bigImagePath: pictureData["bigImagePath"].stringValue, image: nil)
          let pictureURL = URL(string: picture.smallImagePath)!
          let imageData = try! Data(contentsOf: pictureURL)
          let image = UIImage(data: imageData)!
          picture.image = image
          pictures.append(picture)
        }
        completion(true, pictures)
        break
      case .failure(let error):
        print("Failed fetching pictures: \(error.localizedDescription)")
        completion(false, nil)
        break
      }
    }
  }
  
  func getGIFWith(weather: String? = nil, completionHandler completion: @escaping (Bool, Data?) -> Void) {
    self.sessionManager.request("\(DTNetworkManager.apiAddress)/gif", method: .get).validate(statusCode: [200]).responseJSON(queue: DispatchQueue.global(qos: .background)) { response in
      switch response.result {
      case .success(let value):
        let responseData = JSON(value)
        let gifPath = responseData["gif"].stringValue
        let gifURL = URL(string: gifPath)!
        let gifData = try? Data(contentsOf: gifURL)
        guard let data = gifData else {
          print("Failed getting GIF data from server")
          completion(false, nil)
          return
        }
        completion(true, data)
        break
      case .failure(let error):
        print("Failed getting GIF: \(error.localizedDescription)")
        completion(false, nil)
        break
      }
    }
  }
  
  func uploadImage(_ imageURL: URL, withLatitude latitude: Double, longitude: Double, description: String? = nil, hashtag: String? = nil, completionHandler completion: @escaping (Bool) -> Void) {
    var params: [String: String] = [:]
    params["latitude"] = latitude.description
    params["longitude"] = longitude.description
    if let description = description {
      params["description"] = description
    }
    if let hashtag = hashtag {
      params["hashtag"] = hashtag
    }
    
    let headers = ["Content-type": "multipart/form-data"]
    
    self.sessionManager.upload(multipartFormData: { (multipartData) in
      for (key, value) in params {
        multipartData.append(value.data(using: String.Encoding.utf8)!, withName: key)
      }
      multipartData.append(imageURL, withName: "image")
    }, to: "\(DTNetworkManager.apiAddress)/image", method: .post, headers: headers) { (result) in
      switch result {
      case .success(let request, _, _):
        request.validate(statusCode: [201]).responseJSON() { response in
          switch (response.result) {
          case .success(let value):
            completion(true)
            break
          case .failure(let error):
            if ((response.response?.statusCode)! == 400) {
              let errorData = JSON(response.data!)
              if let latitudeErrors = errorData["children"]["latitude"]["errors"].array {
                for errorString in latitudeErrors {
                  print(errorString.stringValue)
                }
              }
              if let longitudeErrors = errorData["children"]["longitude"]["errors"].array {
                for errorString in longitudeErrors {
                  print(errorString.stringValue)
                }
              }
              if let imageErrors = errorData["children"]["image"]["errors"].array {
                for errorString in imageErrors {
                  print(errorString.stringValue)
                }
              }
            } else {
              print("Response status: \((response.response?.statusCode)!), \(error.localizedDescription)")
            }
            completion(false)
            break
          }
        }
        break
      case .failure(let error):
        print("Failed encoding data for image upload: \(error.localizedDescription)")
        completion(false)
        break
      }
    }
  }
}
