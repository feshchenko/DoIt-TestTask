//
//  DTAddImageViewController.swift
//  DoIt-TestTask
//
//  Created by Arthur Feshchenko on 10/1/17.
//  Copyright © 2017 Arthur Feshchenko. All rights reserved.
//

import UIKit
import CoreLocation
import Photos

class DTAddImageViewController: UIViewController {
  
  @IBOutlet var pictureButton: UIButton!
  @IBOutlet var descriptionField: UITextField!
  @IBOutlet var hashtagField: UITextField!
  
  let networkManager = DTNetworkManager.instance
  var pictureURL: URL?
  var locationManager = CLLocationManager()
  var location: CLLocation?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.locationManager.delegate = self
    if (PHPhotoLibrary.authorizationStatus() == .notDetermined) {
      PHPhotoLibrary.requestAuthorization() { status in
        
      }
    }
    if (CLLocationManager.authorizationStatus() == .notDetermined) {
      self.locationManager.requestWhenInUseAuthorization()
    }
    if (CLLocationManager.locationServicesEnabled()) {
      self.locationManager.startUpdatingLocation()
    }
  }
  
  @IBAction func pictureButtonPressed() {
    if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
      let pickerController = UIImagePickerController()
      pickerController.delegate = self
      pickerController.sourceType = .photoLibrary
      pickerController.allowsEditing = false
      pickerController.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
      self.present(pickerController, animated: true, completion: nil)
    }
  }
  
  @IBAction func doneButtonPressed() {
    if let url = self.pictureURL {
      
      if (PHPhotoLibrary.authorizationStatus() == .authorized) {
        let opts = PHFetchOptions()
        opts.fetchLimit = 1
        let assets = PHAsset.fetchAssets(withALAssetURLs: [url], options: opts)
        let asset = assets.firstObject
        self.location = asset?.location
      }
      
      if (self.location == nil) {
        self.location = self.locationManager.location
      }
      
      if (self.location == nil) {
        self.networkManager.uploadImage(url, withLatitude: 0.0, longitude: 0.0, description: self.descriptionField.text, hashtag: self.hashtagField.text) { result in
          self.navigationController?.popViewController(animated: true)
        }
      } else {
        self.networkManager.uploadImage(url, withLatitude: self.location!.coordinate.latitude, longitude: self.location!.coordinate.longitude, description: self.descriptionField.text, hashtag: self.hashtagField.text) { result in
          self.navigationController?.popViewController(animated: true)
        }
      }
      
    }
  }
}

//MARK: - UIImagePickerControllerDelegate, UINavigationControllerDelegate
extension DTAddImageViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    let image = info[UIImagePickerControllerOriginalImage] as? UIImage
    self.pictureURL = info[UIImagePickerControllerImageURL] as? URL
    self.pictureButton.setBackgroundImage(image, for: .normal)
    picker.dismiss(animated: true, completion: nil)
  }
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    picker.dismiss(animated: true, completion: nil)
  }
}

//MARK: - CLLocationManagerDelegate
extension DTAddImageViewController: CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    if (status == .authorizedWhenInUse) {
      manager.startUpdatingLocation()
    }
  }
}


