//
//  DTLoginViewController.swift
//  DoIt-TestTask
//
//  Created by Arthur Feshchenko on 9/29/17.
//  Copyright © 2017 Arthur Feshchenko. All rights reserved.
//

import UIKit

class DTLoginViewController: UIViewController {
  @IBOutlet var avatarButton: UIButton!
  @IBOutlet var usernameField: UITextField!
  @IBOutlet var emailField: UITextField!
  @IBOutlet var passwordField: UITextField!
  
  let networkManager = DTNetworkManager.instance
  var avatarURL: URL?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    let userDefaults = UserDefaults.standard
    if let url = userDefaults.url(forKey: "avatarURL") {
      let avatarData = try? Data(contentsOf: url)
      if let data = avatarData {
        let image = UIImage(data: data)
        avatarButton.setBackgroundImage(image, for: .normal)
        return
      }
    }
    self.avatarURL = Bundle.main.url(forResource: "default-avatar", withExtension: "jpg")!
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    avatarButton.layer.masksToBounds = true
    avatarButton.layer.cornerRadius = (avatarButton.bounds.height / 2)
  }
  
  @IBAction func avatarButtonPressed() {
    if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
      let pickerController = UIImagePickerController()
      pickerController.delegate = self
      pickerController.sourceType = .photoLibrary
      pickerController.allowsEditing = false
      pickerController.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
      self.present(pickerController, animated: true, completion: nil)
    }
  }
  
  @IBAction func sendButtonPressed() {
    let userDefaults = UserDefaults.standard
    
    if (userDefaults.string(forKey: "token") != nil) {
      self.networkManager.loginWith(email: self.emailField.text!,password: self.passwordField.text!) { (result, token, avatarURL) in
        if (result) {
          let url = URL(string: avatarURL!)!
          userDefaults.set(url, forKey: "avatarURL")
          userDefaults.set(token!, forKey: "token")
          self.performSegue(withIdentifier: "showImages", sender: self)
        }
      }
    } else {
      self.networkManager.registerWith(email: self.emailField.text!, password: self.passwordField.text!, avatar: self.avatarURL!) { (result, token, avatarURL) in
        if (result) {
          let url = URL(string: avatarURL!)!
          userDefaults.set(url, forKey: "avatarURL")
          userDefaults.set(token!, forKey: "token")
          self.performSegue(withIdentifier: "showImages", sender: self)
        }
      }
    }
  }
}

//MARK: - UIImagePickerControllerDelegate, UINavigationControllerDelegate
extension DTLoginViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    let image = info[UIImagePickerControllerOriginalImage] as? UIImage
    self.avatarURL = info[UIImagePickerControllerImageURL] as? URL
    self.avatarButton.setBackgroundImage(image, for: .normal)
    picker.dismiss(animated: true, completion: nil)
  }
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    picker.dismiss(animated: true, completion: nil)
  }
}
