//
//  DTImagesViewController.swift
//  DoIt-TestTask
//
//  Created by Arthur Feshchenko on 10/1/17.
//  Copyright © 2017 Arthur Feshchenko. All rights reserved.
//

import UIKit
import Gifu

class DTImagesViewController: UIViewController {
  
  @IBOutlet var collectionView: UICollectionView!
  @IBOutlet var popOverView: UIView!
  @IBOutlet var gifImageView: GIFImageView!
  
  let networkManager = DTNetworkManager.instance
  var pictures: [DTPicture] = []
  var navigationZIndex: CGFloat?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.isNavigationBarHidden = false
    self.navigationItem.hidesBackButton = true
    self.collectionView.dataSource = self
    let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(hidePopOver))
    self.popOverView.addGestureRecognizer(tapRecognizer)
    self.gifImageView.addGestureRecognizer(tapRecognizer)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super .viewWillAppear(animated)
    self.networkManager.getImagesWith() { result, pictures in
      if (result) {
        self.pictures = pictures!
        DispatchQueue.main.async {
          self.collectionView.reloadData()
        }
      }
    }
  }
  
  @objc func hidePopOver() {
    self.navigationController?.navigationBar.layer.zPosition = self.navigationZIndex!
    self.popOverView.isHidden = true
  }
  
  @IBAction func gifButtonPressed() {
    self.networkManager.getGIFWith() { result, gifData in
      if (result) {
        DispatchQueue.main.async {
          self.gifImageView.animate(withGIFData: gifData!)
          self.navigationZIndex = self.navigationController?.navigationBar.layer.zPosition
          self.navigationController?.navigationBar.layer.zPosition = self.popOverView.layer.zPosition - 1
          self.popOverView.isHidden = false
        }
      }
    }
  }
}

//MARK: - UICollectionViewDataSource
extension DTImagesViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.pictures.count
  }
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PictureCell", for: indexPath) as! DTPictureCollectionViewCell
    let model = pictures[indexPath.row]
    cell.hashtag.text = model.hashtag
    cell.weather.text = model.weather
    cell.imageView.image = model.image
    
    return cell
  }
}
