//
//  DTPictureCollectionViewCell.swift
//  DoIt-TestTask
//
//  Created by Arthur Feshchenko on 10/1/17.
//  Copyright © 2017 Arthur Feshchenko. All rights reserved.
//

import UIKit

class DTPictureCollectionViewCell: UICollectionViewCell {
  @IBOutlet var weather: UILabel!
  @IBOutlet var hashtag: UILabel!
  @IBOutlet var imageView: UIImageView!
}
